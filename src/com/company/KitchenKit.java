package com.company;

public enum KitchenKit {

    CoffeeMachine,
    WaterDispenser,
    Fridge,
    SandwichMaker,
    Furniture

}
