package com.company;


import java.util.ArrayList;

import java.util.List;


public class Floor extends Building {

    private String buildingName;
    private String floorName;

    private List<Rooms> roomList = new ArrayList<>();


    public Floor(String buildingName, String floorName) {
        this.buildingName = buildingName;
        this.floorName = floorName;
    }

    @Override
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    public Floor() {

    }

    public String getBuildingName() {

        return buildingName;
    }

    public String getFloorName() {

        return floorName;
    }

    public int getRoomsNumber() {

        return roomList.size();
    }

    public String toString() {

        return getFloorName() + ":";
    }

}
