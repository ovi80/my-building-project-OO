package com.company;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;


public class Main {

    static Building building = new Building();

    public static void main(String[] args) {

        String buildingName;

        int validationWhileBuilding;

        do {

            validationWhileBuilding = JOptionPane.showConfirmDialog(null, "Do you want to add a building ?", "Building Window", JOptionPane.YES_NO_OPTION);
            if (validationWhileBuilding == JOptionPane.YES_OPTION) {
                do {
                    buildingName = JOptionPane.showInputDialog("Please enter Building name !!!");
                    if (buildingName.length() == 0) {
                        JOptionPane.showMessageDialog(null, " Empty field !!! ");
                    }
                } while (buildingName.length() == 0);

                addBuilding(buildingName);

            } else {
                JOptionPane.showMessageDialog(null, "GOODBYE");
            }
        } while (validationWhileBuilding == JOptionPane.YES_OPTION);


        System.out.println(building.toString());

    }

    public static int typeOfRoomsNumber() {

        int lengtth = 0;

        for (TypeOfRooms t : TypeOfRooms.values()) {
            lengtth++;
        }

        return lengtth;
    }

    public static int kitchenAplianceNumber() {

        int lengtth = 0;

        for (KitchenKit k : KitchenKit.values()) {
            lengtth++;
        }

        return lengtth;
    }

    public static Map<Integer, String> kitchenKitMap() {

        Map<Integer, String> kitchenKitMap = new HashMap<>();

        int i = 0;

        for (KitchenKit k : KitchenKit.values()) {

            kitchenKitMap.put(i, k.name());

            i++;

        }

        return kitchenKitMap;
    }


    public static String[] typeOfRoomlsList() {

        String[] typeOfRoomsList = new String[typeOfRoomsNumber()];

        int i = 0;

        for (TypeOfRooms t : TypeOfRooms.values()) {
            typeOfRoomsList[i] = t.name();
            i++;
        }
        return typeOfRoomsList;
    }

    public static void addBuilding(String buildingName) {

        int validationWhileFlor;
        String floorName;

        building.addBuilding(buildingName);


        do {
            validationWhileFlor = JOptionPane.showConfirmDialog(null, "Do you want to add a floor ?", "Floor Window", JOptionPane.YES_NO_OPTION);
            if (validationWhileFlor == JOptionPane.YES_OPTION) {
                do {
                    floorName = JOptionPane.showInputDialog("Please enter Floor name !!!");
                    if (floorName.length() == 0) {
                        JOptionPane.showMessageDialog(null, " Empty field !!! ");
                    }
                } while (floorName.length() == 0);

                addFloor(buildingName, floorName);
            }

        } while (validationWhileFlor == JOptionPane.YES_OPTION);

    }

    public static void addFloor(String buildingName, String floorName) {

        int validationWhileRoom;

        building.addFloor(buildingName, floorName);


        do {
            validationWhileRoom = JOptionPane.showConfirmDialog(null, "Do you want to add a room ?", "Room Window", JOptionPane.YES_NO_OPTION);
            if (validationWhileRoom == JOptionPane.YES_OPTION) {

                addRoom(buildingName, floorName);

            }

        } while (validationWhileRoom == JOptionPane.YES_OPTION);

    }

    public static void addRoom(String buildingName, String floorName) {

        String typeOfRoomsValue;
        String roomName;
        String inputParse;

        int numberOfDesks = 0;
        int numberOfSeats = 0;
        int validationKitchenKit;

        boolean isKitchenKit;
        boolean validationNumberOfDesks;
        boolean validationnumberOfSeats;

        String[] choises = typeOfRoomlsList();

        Map<Integer, String> kitchenKitAplianceMap = new HashMap<>();

        typeOfRoomsValue = (String) JOptionPane.showInputDialog(null, "Choose now...", "Room type", JOptionPane.QUESTION_MESSAGE, null, choises, choises[0]);

        do {
            roomName = JOptionPane.showInputDialog("Please enter Room name !!!");
            if (roomName.length() == 0) {
                JOptionPane.showMessageDialog(null, " Empty field !!! ");
            }
        } while (roomName.length() == 0);

        if (typeOfRoomsValue.equals(TypeOfRooms.Toilet.name())) {
            numberOfDesks = 0;
        } else {

            do {
                inputParse = JOptionPane.showInputDialog("Please enter number of desks !!!");
                if (inputParse.length() == 0) {
                    JOptionPane.showMessageDialog(null, " Empty field !!! ");
                    validationNumberOfDesks = true;
                } else {
                    try {
                        numberOfDesks = Integer.parseInt(inputParse);
                        validationNumberOfDesks = false;
                        if (numberOfDesks < 0 || numberOfDesks > 100) {
                            JOptionPane.showMessageDialog(null, " Please enter an INTEGER between 0 and 99!!! ");
                            validationNumberOfDesks = true;
                        }
                    } catch (NumberFormatException e) {
                        validationNumberOfDesks = true;
                        JOptionPane.showMessageDialog(null, " Please enter an INTEGER !!! ");
                    }
                }
            } while (validationNumberOfDesks);
        }


        if (typeOfRoomsValue.equals(TypeOfRooms.Toilet.name())) {
            numberOfSeats = 0;
        } else {
            do {
                inputParse = JOptionPane.showInputDialog("Please enter number of seats !!!");
                if (inputParse.length() == 0) {
                    JOptionPane.showMessageDialog(null, " Empty field !!! ");
                    validationnumberOfSeats = false;
                } else {
                    try {
                        numberOfSeats = Integer.parseInt(inputParse);
                        validationnumberOfSeats = false;
                        if (numberOfSeats < 0 || numberOfSeats > 100) {
                            JOptionPane.showMessageDialog(null, " Please enter an INTEGER between 0 and 99!!! ");
                            validationnumberOfSeats = true;
                        }
                    } catch (NumberFormatException e) {
                        validationnumberOfSeats = true;
                        JOptionPane.showMessageDialog(null, " Please enter an INTEGER !!! ");
                    }
                }
            } while (validationnumberOfSeats);
        }

        if (typeOfRoomsValue.equals(TypeOfRooms.Toilet.name())) {
            isKitchenKit = false;
        } else {
            validationKitchenKit = JOptionPane.showConfirmDialog(null, "Does the room have a kitchen kit?", "Kitchen Kit Window", JOptionPane.YES_NO_OPTION);
            if (validationKitchenKit == JOptionPane.YES_OPTION) {
                isKitchenKit = true;

                String[] kitchenAplianceList = new String[kitchenKitMap().size()];

                for (int i = 0; i < kitchenKitMap().size(); i++) {
                    kitchenAplianceList[i] = kitchenKitMap().get(i);
                }

                JCheckBox[] check = new JCheckBox[kitchenAplianceList.length];

                for (int i = 0; i < kitchenAplianceList.length; i++)
                    check[i] = new JCheckBox(kitchenAplianceList[i]);

                boolean[] ret = new boolean[kitchenAplianceList.length];

                int answer = JOptionPane.showConfirmDialog(null, new Object[]{"Choose Kitchen Kit:", check}, "Kitchen Kit", JOptionPane.OK_CANCEL_OPTION);


                if (answer == JOptionPane.OK_OPTION) {
                    for (int i = 0; i < kitchenAplianceList.length; i++)
                        ret[i] = check[i].isSelected();

                } else if (answer == JOptionPane.CANCEL_OPTION || answer == JOptionPane.ERROR_MESSAGE) {
                    for (int i = 0; i < kitchenAplianceList.length; i++)
                        ret[i] = false;
                }


                int counter = 0;

                for (int i = 0; i < kitchenAplianceList.length; i++) {
                    if (ret[i]) {
                        kitchenKitAplianceMap.put(counter, kitchenKitMap().get(i));
                        counter++;
                    }
                }

            } else {
                isKitchenKit = false;
            }
        }

        building.addRoom(typeOfRoomsValue, buildingName, floorName, roomName, numberOfDesks, numberOfSeats, isKitchenKit, kitchenKitAplianceMap);

    }


}



