package com.company;

public enum TypeOfRooms {

    Toilet,
    Kitchen,
    OfficeSpace,
    ConferenceRoom,
    RestRoom,
}
