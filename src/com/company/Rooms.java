package com.company;

import java.util.Map;

public class Rooms {

    private String typeOfRoomsValue;
    private String buildingName;
    private String floorName;
    private String roomName;
    private int numberOfDesks;
    private int numberOfSeats;
    private boolean isKitchenKit;

    private Map<Integer, String> kitchenKit;

    private int floorNumber;

    public Rooms(String typeOfRoomsValue, String buildingName, String floorName, String roomName, int numberOfDesks, int numberOfSeats, boolean isKitchenKit, Map<Integer, String> kitchenKit) {
        this.typeOfRoomsValue = typeOfRoomsValue;
        this.buildingName = buildingName;
        this.floorName = floorName;
        this.roomName = roomName;
        this.numberOfDesks = numberOfDesks;
        this.numberOfSeats = numberOfSeats;
        this.isKitchenKit = isKitchenKit;
        this.kitchenKit = kitchenKit;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public String getFloorName() {
        return floorName;
    }

    public String getRoomName() {
        return roomName;
    }

    public int getNumberOfDesks() {
        return numberOfDesks;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }


    public String getTypeOfRoomsValue() {
        return typeOfRoomsValue;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String afisare() {

        String result = "";

        if (getNumberOfDesks() > 1) {
            result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + getNumberOfDesks() + " desks";
        } else {
            if (getNumberOfDesks() == 1) {
                result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + getNumberOfDesks() + " desk";
            }
        }

        if (getNumberOfSeats() > 1) {
            result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + getNumberOfSeats() + " seats";
        } else {
            if (getNumberOfSeats() == 1) {
                result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + getNumberOfSeats() + " seat";
            }
        }

        if (getTypeOfRoomsValue().equals(TypeOfRooms.ConferenceRoom.name())) {

            if (getNumberOfDesks() <= 20) {
                result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + "A TV";
            } else {
                result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + "A projector";
            }

            if (floorNumber == 3) {
                result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + "Telepresence";
            }
        }

        if (isKitchenKit) {

            for (int i = 0; i < kitchenKit.size(); i++) {

                result += "\n" + "\t" + "\t" + "\t" + "\t" + "\t" + kitchenKit.get(i);
            }

        }

        return result;

    }

    @Override
    public String toString() {
        return "\t" + "\t" + "\t" + "\t" + getRoomName() + ": " + afisare() + "\n";
    }
}
