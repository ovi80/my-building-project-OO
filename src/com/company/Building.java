package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Building {

    private List<String> buildingList = new ArrayList<>();

    private List<Floor> floorList = new ArrayList<>();

    private List<Rooms> roomList = new ArrayList<>();

    private String buildingName;

    private String view = "";

    private String testFloor;

    private String testBuilding;

    private String testRoom;

    private int numberOfFloors = 0;
    private int numbeOfOS = 0;
    private int numberOfToilets = 0;
    private int numberOfKitchens = 0;
    private int numberOfCF = 0;
    private int floorNumber;

    public Building(String buildingName) {

        this.buildingName = buildingName;
    }

    public Building() {

    }

    public String getBuildingName() {

        return buildingName;
    }

    public void setBuildingName(String buildingName) {

        this.buildingName = buildingName;
    }

    public void addBuilding(String buildingName) {

        buildingList.add(buildingName);
    }

    public void addFloor(String buildingName, String floorName) {

        Floor f = new Floor(buildingName, floorName);

        floorList.add(f);

    }

    public void addRoom(String typeOfRoomsValue, String buildingNameC, String floorNameC, String roomName, int numberOfDesks, int numberOfSeats, boolean isKitchenKit, Map<Integer, String> kitchenKit) {

        Rooms newRoom = new Rooms(typeOfRoomsValue, buildingNameC, floorNameC, roomName, numberOfDesks, numberOfSeats, isKitchenKit, kitchenKit);
        roomList.add(newRoom);

    }


    public String afisare() {


        if (buildingList.size() == 0) {
            view = "No building is created!!!";
        } else {


            for (String b : buildingList) {


                testBuilding = b;

                numberOfFloors = 0;

                floorNumber = 0;

                view += "Building " + testBuilding + " has ";

                for (Floor s0 : floorList) {

                    if (s0.getBuildingName().equals(testBuilding)) {

                        numberOfFloors++;
                    }

                }

                if (numberOfFloors > 1) {
                    view += numberOfFloors + " floors" + "\n";
                } else {
                    if (numberOfFloors == 1) {
                        view += numberOfFloors + " floor" + "\n";
                    } else {
                        view += "no floors" + "\n";
                    }
                }

                for (Floor s : floorList) {

                    if (s.getBuildingName().equals(testBuilding)) {

                        numbeOfOS = 0;
                        numberOfToilets = 0;
                        numberOfKitchens = 0;
                        numberOfCF = 0;

                        floorNumber++;

                        testFloor = s.getFloorName();


                        view += s.toString() + "\n";


                        for (TypeOfRooms t : TypeOfRooms.values()) {

                            int numberOfRoom = 0;

                            for (Rooms room : roomList) {

                                testRoom = t.name();

                                if (room.getBuildingName().equals(testBuilding) && room.getFloorName().equals(testFloor) && room.getTypeOfRoomsValue().equals(t.name())) {

                                    numberOfRoom++;
                                }
                            }


                            if (numberOfRoom == 1) {
                                view += "\t" + "\t" + numberOfRoom + " " + testRoom + "\n";
                            } else {
                                if (numberOfRoom > 1) {
                                    view += "\t" + "\t" + numberOfRoom + " " + testRoom + "s" + "\n";
                                }
                            }

                            for (Rooms room : roomList) {

                                if (room.getBuildingName().equals(testBuilding) && room.getFloorName().equals(testFloor) && room.getTypeOfRoomsValue().equals(t.name())) {

                                    room.setFloorNumber(floorNumber);

                                    view += room.toString();
                                }
                            }


                        }

                    }
                }
            }
        }

        return view;
    }

    @Override
    public String toString() {

        return afisare();

    }


}
